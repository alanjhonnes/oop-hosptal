/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modal.Agenda;

import Controller.ControllerMedico;
import Controller.ControllerPaciente;
import Model.Pessoa.Medico;
import Model.Pessoa.Paciente;

/**
 *
 * @author aramos
 */
public class Agendamento {

    public String agendaConsulta(int _dia, int _mes, String _cpfMedico, String _cpfPaciente, int _periodo) {
        ControllerMedico medico =  new ControllerMedico();
        ControllerPaciente paciente = new ControllerPaciente();
        Medico m = medico.consultCpfMedico(_cpfMedico);
        Paciente pi = paciente.consultPaciente(_cpfPaciente);
        if (m == null) return "O MÉDICO NÃO CONSTA NOS REGISTROS";
        if (pi == null) return "O PACIENTE NÃO CONSTA NOS REGISTROS";
        CreateCalendar calendar = new CreateCalendar();
        Periodo[] p = new Periodo[3];
        String type = null; 
        if (_mes >12) return "INFORME UM MES VALIDO";
        if(_dia >31)return "INFORME UM DIA VALIDO";
        if (_mes < calendar.checkMesAtual()) return "INFORME UM MES VALIDO";
        String diaSemana = calendar.dias(2013, _mes, _dia);
        if ((diaSemana.equalsIgnoreCase("Sábado") || (diaSemana.equalsIgnoreCase("Domingo")))) {
          return "DIA NÃO DISPONIVEL PARA CONSULTA!";  
        }
        switch (_periodo) {
            case 1:
                type = "Manha";
            break;
            case 2:
                type= "Tarde";
            break;
            case 3:
                type= "Noite";
            break;
            default:
                return "INFORME UM PERIODO VALIDO;";
        }
        
//        p[_periodo -1] = new Periodo(type, , m);
        
        return "CONSULTA AGENDADA COM SUCESSO!";
    }
    
    public Periodo consultMedico(){
        

        return null;
    
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modal.Agenda;

import Model.Pessoa.Medico;
import Model.Pessoa.Paciente;
/**
 *
 * @author aramos
 */
public class Periodo {
    private String horario;
    private Paciente paciente;
    private Medico medico;

    public Periodo(String horario, Paciente paciente, Medico medico) {
        this.horario = horario;
        this.paciente = paciente;
        this.medico = medico;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    
}

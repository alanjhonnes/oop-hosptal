/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modal.Agenda;

import java.util.ArrayList;

/**
 *
 * @author Wellington
 */
public class Agenda {
    
    private int dia;
    private int mes;
    private String periodo;
    private String cpfMedico;
    private String cpfPaciente;

    //    private ArrayList<Medico> medico;
    public Agenda(int dia, int mes, String periodo, String cpfMedico, String cpfPaciente) {
        this.dia = dia;
        this.mes = mes;
        this.periodo = periodo;
        this.cpfMedico = cpfMedico;
        this.cpfPaciente = cpfPaciente;
    }
    
    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getCpfMedico() {
        return cpfMedico;
    }

    public void setCpfMedico(String cpfMedico) {
        this.cpfMedico = cpfMedico;
    }

    public String getCpfPaciente() {
        return cpfPaciente;
    }

    public void setCpfPaciente(String cpfPaciente) {
        this.cpfPaciente = cpfPaciente;
    }
    
    
}

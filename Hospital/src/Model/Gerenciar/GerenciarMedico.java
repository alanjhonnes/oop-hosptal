/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Gerenciar;

import Model.Pessoa.Medico;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aramos
 */
public class GerenciarMedico {
    
    private List<Medico> medicos;

    public GerenciarMedico() {
        medicos = new ArrayList<>();
    }
    
    public String cadastroMedico(Medico medico){
        if ((medico.getCrm().isEmpty())&&(medico.getCpf().isEmpty())) {
            return "POR FAVOR, INFORME O CRM/CPF";
        }
        
        medicos.add(medico);
        return "CADASTRO REALIZADO COM SUCESSO!";
    }
    public Medico consultMedicoCpf(String cpf){
        int medicoSize =medicos.size();
        for (int i = 0; i < medicoSize; i++) {
            Medico m =medicos.get(i);
            if (m.getCpf().equalsIgnoreCase(cpf)) {
                return m;
            }
        }
        return null;
    }
}

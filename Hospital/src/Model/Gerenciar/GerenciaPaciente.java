/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Gerenciar;

import Model.Pessoa.Paciente;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alberto
 */
public class GerenciaPaciente {

    private List<Paciente> pacientes;

    public GerenciaPaciente() {
        pacientes = new ArrayList<>();
    }

    public String cadastroPaciente(Paciente paciente) {
        if ((paciente.getId() < 0) && (paciente.getCpf().isEmpty())) {
            return "POR FAVOR, INFORME O id/CPF";
        }
        pacientes.add(paciente);
        return "CADASTRO REALIZADO COM SUCESSO!";
    }
    public Paciente consultPaciente(String _cpf){
        int pacienteSize = pacientes.size();
        for (int i = 0; i < pacienteSize; i++) {
            Paciente p = pacientes.get(i);
            if (p.getCpf().equalsIgnoreCase(_cpf)) {
                return p;
            }
        }
        return null;
    }
}

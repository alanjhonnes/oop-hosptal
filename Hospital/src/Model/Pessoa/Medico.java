/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Pessoa;

import Modal.Agenda.Dia;
import java.util.List;

/**
 *
 * @author Wellington
 */
public class Medico extends Pessoa {

    private String crm;
    private List<Dia> dia;
    
    public Medico() {
    }
    
    public Medico(String crm, String nome, int idade, String cpf, String endereço, String telefone, String email) {
        super(nome, idade, cpf, endereço, telefone, email);
        this.crm = crm;
    }
    
    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public List<Dia> getDia() {
        return dia;
    }

    public void setDia(List<Dia> dia) {
        this.dia = dia;
    }

    @Override
    public String toString() {
        return "Crm: " + crm + super.toString()+
                "dia: " + dia;
    }

   
}

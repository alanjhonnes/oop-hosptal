/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Pessoa;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Wellington
 */

public class Paciente extends Pessoa {
    
    private int id;
    private String dataInternacao;

    public Paciente(int id, String dataInternacao, String nome, int idade, String cpf, String endereço, String telefone, String email) {
        super(nome, idade, cpf, endereço, telefone, email);
        this.id = id;
        this.dataInternacao = dataInternacao;
    }

    public Paciente(int id, String nome, int idade, String cpf, String endereço, String telefone, String email, String dataInternacao) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDataInternacao() {
        return dataInternacao;
    }

    public void setDataInternacao(String dataInternacao) {
        this.dataInternacao = dataInternacao;
    }

}

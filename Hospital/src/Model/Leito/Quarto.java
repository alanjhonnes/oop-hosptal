/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Leito;


/**
 *
 * @author aramos
 */
public class Quarto {
    private int id;
    private Leito[] leito;

    public Quarto(int id, Leito[] leito) {
        this.id = id;
        this.leito = leito;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Leito[] getLeito() {
        return leito;
    }

    public void setLeito(Leito[] leito) {
        this.leito = leito;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Leito;

import Model.Pessoa.Medico;
import Model.Pessoa.Paciente;

/**
 *
 * @author aramos
 */
public class Leito {
    private String cod;
    private Medico medico;
    private Paciente paciente;

    public Leito(String cod, Medico medico, Paciente paciente) {
        this.cod = cod;
        this.medico = medico;
        this.paciente = paciente;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
    
}

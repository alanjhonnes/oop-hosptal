/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Gerenciar.GerenciaPaciente;
import Model.Pessoa.Paciente;
import Views.TelaCadastroPaciente;

/**
 *
 * @author alberto
 */
public class ControllerPaciente {
     private TelaCadastroPaciente cadastroPaciente;
    private GerenciaPaciente cadPaciente;

    public ControllerPaciente() {
        this.cadastroPaciente = new TelaCadastroPaciente();
        this.cadastroPaciente.cadastroPaciente();
        cadPaciente = new GerenciaPaciente();
    }
    public String cadastroPaciente (int id, String nome, int idade, String cpf,
            String endereço, String telefone, String email, String dataInternacao){
        
       Paciente paciente = new Paciente (id,nome,idade,cpf,endereço,telefone,email,dataInternacao);
       return cadPaciente.cadastroPaciente(paciente);
    }
    public Paciente consultPaciente(String _cpf){
        return cadPaciente.consultPaciente(_cpf);
    }
    
    
}

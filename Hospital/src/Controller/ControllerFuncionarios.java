/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Gerenciar.GerenciarFuncionarios;
import Views.TelaCadastroFuncionario;
import Model.Pessoa.Funcionarios;

/**
 *
 * @author alberto
 */
public class ControllerFuncionarios {
    
     private TelaCadastroFuncionario cadastroFuncionario;
    private GerenciarFuncionarios cadFuncionarios;

    public ControllerFuncionarios() {
        this.cadastroFuncionario = new TelaCadastroFuncionario();
        this.cadastroFuncionario.cadastroFuncionarios();
        cadFuncionarios = new GerenciarFuncionarios();
    }
    public String cadastroFuncionarios (String nome, int idade, String cpf,
            String endereço, String telefone, String email, String deptAtuacao){
        
       Funcionarios funcionario = new Funcionarios (nome,idade,cpf,endereço,telefone,email,deptAtuacao);
       return cadFuncionarios.cadastroFuncionarios(funcionario);
    
}
}

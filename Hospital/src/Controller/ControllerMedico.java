/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Gerenciar.GerenciarMedico;
import Model.Pessoa.Medico;
import Views.TelaCadastroMedico;

/**
 *
 * @author aramos
 */
public class ControllerMedico {

    private TelaCadastroMedico cadastroMedico;
    private GerenciarMedico cadMedico;
    
    public ControllerMedico() {
        this.cadastroMedico = new TelaCadastroMedico(this);
        this.cadastroMedico.cadastroMedico();
        
    }
    public String cadastroMedico(String crm, String nome, int idade, String cpf,
            String endereço, String telefone, String email){
        cadMedico = new GerenciarMedico();
        Medico medico = new Medico(crm, nome, idade, cpf, endereço, telefone, email);
        return cadMedico.cadastroMedico(medico);
        
    }
    public Medico consultCpfMedico(String _cpf){
        return cadMedico.consultMedicoCpf(_cpf); 
    }
    
}

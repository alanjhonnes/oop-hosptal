/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Views.TelaInicial;

/**
 *
 * @author alberto
 */
public class ControllerInicial {

    private TelaInicial telaInicial;
    
    public ControllerInicial() {
        this.telaInicial = new TelaInicial(this);
        this.telaInicial.MenuInicial();
    }
    public void callMedic(){
       ControllerMedico control = new ControllerMedico();
    }
    public void callAgenda(){
        ControllerAgenda control = new ControllerAgenda();
    }
    public void callPatient(){
        ControllerPaciente control = new ControllerPaciente();
    }
    public void callFuncionarios(){
        ControllerFuncionarios control = new ControllerFuncionarios();
    }
}
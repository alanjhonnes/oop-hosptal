/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import Controller.ControllerMedico;
import java.util.Scanner;

/**
 *
 * @author aramos
 */
public class TelaCadastroMedico {
    // TODO code application logic here

    private Controller.ControllerMedico controllerMedico;

    public TelaCadastroMedico(ControllerMedico controllerMedico) {
        this.controllerMedico = controllerMedico;
    }
    
    public void cadastroMedico() {
        Scanner scan = new Scanner(System.in);
        try {
            String input;
            String crm;
            String nome;
            int idade;
            String cpf;
            String endereco;
            String telefone;
            String email;
            System.out.println("== Cadastrar Médico ==");
            System.out.printf("CRM: ");
            crm = scan.nextLine();
            System.out.printf("Nome: ");
            nome = scan.nextLine();
            System.out.printf("Idade: ");
            input = scan.nextLine();
            idade = Integer.parseInt(input);
            System.out.printf("Cpf: ");
            cpf = scan.nextLine();
            System.out.printf("Endereco: ");
            endereco = scan.nextLine();
            System.out.printf("Telefone: ");
            telefone = scan.nextLine();
            System.out.println("Email: ");
            email = scan.nextLine();
            String resurt = controllerMedico.cadastroMedico(crm, nome, idade, cpf, endereco, telefone, email);
            System.out.println(resurt);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}

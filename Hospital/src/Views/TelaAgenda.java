/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import java.util.Scanner;
import Controller.ControllerAgenda;
/**
 *
 * @author aramos
 */
public class TelaAgenda {
    ControllerAgenda comAgenda;

    public TelaAgenda(ControllerAgenda c) {
        comAgenda = c;
    }
    

    public void telaAgenda() {

        Scanner scan = new Scanner(System.in);

        try {
            int dia;
            int mes;
            int periodo = 4;
            String input;
            String cpfPaciente;
            String cpfMedico;
            System.out.println("== Agendar Consultas ==");
            System.out.printf("Informe o dia para consulta: ");
            input = scan.nextLine();
            dia = Integer.parseInt(input);
            System.out.printf("Informe o mes para consulta: ");
            input = scan.nextLine();
            mes = Integer.parseInt(input);
            while (periodo == 4) {

                System.out.println("== Selecione o periodo ==");
                System.out.println("1. Manha.");
                System.out.println("2. Tarde.");
                System.out.println("3. Noite.");

                System.out.println("Digite a opcao desejada:");
                input = scan.nextLine();
                periodo = Integer.parseInt(input);

                switch (periodo) {
                    case 1:
                        periodo = 1;
                        break;

                    case 2:
                        periodo = 2;
                        break;

                    case 3:
                        periodo = 3;
                        break;

                }
            }
            System.out.printf("Informe o cpf do paciente: ");
            cpfPaciente = scan.nextLine();
            System.out.printf("Informe o cpf do média a atender a consulta: ");
            cpfMedico = scan.nextLine();
            
            String result = comAgenda.agendarConsulta(dia, mes, cpfMedico, cpfPaciente, cpfMedico);
            System.out.println(result);
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

       
    }

}
